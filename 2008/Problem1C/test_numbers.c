
#include "numbers.h"

#include <stdio.h>

int main() {
    int *period_info = find_period();

    printf("A_%d and A_%d vs. A_%d and A_%d\n", period_info[0] - 1, period_info[0], period_info[1] - 1, period_info[1]);
    printf("(%d, %d) compared to (%d, %d)\n", A_mod_1000_term(period_info[0] - 1), A_mod_1000_term(period_info[0]), A_mod_1000_term(period_info[1] - 1), A_mod_1000_term(period_info[1]));
}
