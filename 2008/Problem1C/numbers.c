
#include "numbers.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int A_mod_1000(int one_back, int two_back) {
    int current_term = (6*one_back - 4*two_back) % 1000;
    if (current_term <= 0) {
        current_term = current_term + 1000;
    }

    return current_term;
}



int A_mod_1000_term(long int n) {
    int A[2] = {2, 6};
    int term;

    if (n == 0) {
        return A[0];
    } else if (n == 1) {
        return A[1];
    }

    long int i;
    for (i = 2; i <= n; i++) {
        term = A_mod_1000(A[1], A[0]);
        A[0] = A[1];
        A[1] = term;
    }

    return term;
}



int* find_period() {
    int terms[1000001];
    terms[0] = 2;
    terms[1] = 6;

    int i, j, period, finished;
    finished = 0;
    for (i = 2; i < 1000001; i++) {
        printf("%d/1000000\n", i);
        terms[i] = A_mod_1000(terms[i-1], terms[i-2]);

        for (j = 1; j < i - 1; j++) {
            if (terms[i] == terms[j] && terms[i-1] == terms[j-1]) {
                finished = j;
                break;
            }
        }

        if (finished != 0) {
            period = i;
            break;
        }
    }

    int *results= malloc(sizeof(int)*2);
    results[0] = finished;
    results[1] = period;
    return results;
}
