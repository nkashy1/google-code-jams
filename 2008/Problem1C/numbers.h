
/******************************************************************

Google Code Jam 2008, Problem C: Numbers
-------------------------------------------------------

We are given an integer n and want to find the last three digits (hundreds, tens, and ones digits) in the decimal expansion of the number (3 + sqrt(5))^n.

Input:  We receive test cases in text files.
        The first line of each file contains the number T of test cases in the file.
        Each test case consists of one positive integer n for which we need to solve the given problem.

Output: For each test case, the program should output a line
        "Case #k: digits",
        where k is the number of the test case and digits are the last three digits before the decimal point in (3 + sqrt(5))^n.

Data sets:  (1) The small data set consists of 1 <= T <= 100 test cases each with 2 <= n <= 30.
            (2) The large data set consists of 1 <= T <= 100 test cases each woth 2 <= n <= 2,000,000,000.

*******************************************************************/


// We will solve the problem by calculating modulo 1000 the terms of the sequence A_n with A_0 = 2, A_1 = 6, and which satisfies the recurrence A_n = 6A_{n-1} - 4A_{n-2}, which corresponds to the minimal polynomial of 3 + sqrt(5).

int A_mod_1000(int one_back, int two_back);

// generate_terms returns term A_n modulo 1000. (Smallest positive integer in congruence class.)
int A_mod_1000_term(long int n);

// find_period finds the period of the iterations modulo 1000
int* find_period();
