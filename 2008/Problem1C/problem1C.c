
#include "numbers.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>



// next_int returns the next integer from a file of integers.
int next_int(FILE* ifp) {
    int value;
    fscanf(ifp, "%d", &value);
    return value;
}



// next_long_int returns the next long integer from a file of integers.
long int next_long_int(FILE* ifp) {
    long int value;
    fscanf(ifp, "%ld", &value);
    return value;
}



int main(int argc, char* argv[]) {
    assert(argc == 3);

    char* in_file = argv[1];
    char* out_file = argv[2];

    FILE *ifp = fopen(in_file, "r");
    FILE *ofp = fopen(out_file, "w");

    int T = next_int(ifp);

    int i;
    int table[104];
    for (i = 0; i < 104; i++) {
        table[i] = A_mod_1000_term(i);
    }

    int lookup, digits;
    long int n;
    for (i = 1; i <= T; i++) {
        n = next_long_int(ifp);
        if (n > 3) {
            lookup = (int) ((n - 4)%100);
            if (lookup < 0) {
                lookup += 100;
            }
            lookup += 4;
        } else {
            lookup = (int) n;
        }

        digits = table[lookup];
        if (digits <= 10) {
            fprintf(ofp, "Case #%d: %d%d%d\n", i, 0, 0, digits - 1);
        } else if (digits <= 100) {
            fprintf(ofp, "Case #%d: %d%d\n", i, 0, digits - 1);
        } else {
            fprintf(ofp, "Case #%d: %d\n", i, digits - 1);
        }
    }

    fclose(ifp);
    fclose(ofp);

    return 0;
}
