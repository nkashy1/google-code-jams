
#include "minimum_scalar_product.h"

#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main() {

    char *result;

    /****************************
    ** Test 1
    *****************************/

    printf("\nTest 1...\n");

    int_vector *test_vector, *singleton_1, *singleton_2, *merged_vector;

    test_vector = int_vector_create(2);
    int_vector_enter(test_vector, 0, 1);
    int_vector_enter(test_vector, 1, 0);

    singleton_1 = int_vector_create(1);
    singleton_2 = int_vector_create(1);
    int_vector_enter(singleton_1, 0, 1);
    int_vector_enter(singleton_2, 0, 0);

    int entry_0, entry_1;

    // Test min_int

    result = "Success";

    entry_0 = int_vector_entry(test_vector, 0);
    entry_1 = int_vector_entry(test_vector, 1);

    if (min_int(&entry_0, &entry_1) != 0 || min_int(&entry_1, &entry_0) != 1) {
        result = "Failure";
    }

    printf("min_int: %s\n", result);


    // Test int_vector_merge

    result = "Success";

    merged_vector = int_vector_merge(singleton_1, singleton_2, min_int);

    entry_0 = int_vector_entry(merged_vector, 0);
    entry_1 = int_vector_entry(merged_vector, 1);

    if (entry_0 != 0 || entry_1 != 1) {
      result = "Failure";
    }

    printf("int_vector_merge: %s\n", result);


    // Test int_vector_merge_sort

    result = "Success";

    int_vector *sorted_test_vector = int_vector_merge_sort(test_vector, min_int);

    entry_0 = int_vector_entry(sorted_test_vector, 0);
    entry_1 = int_vector_entry(sorted_test_vector, 1);

    if (entry_0 != 0 || entry_1 != 1) {
        result = "Failure";
	printf("%d, %d\n", entry_0, entry_1);
	printf("%d, %d\n", int_vector_entry(sorted_test_vector, 0), int_vector_entry(sorted_test_vector, 1));
    }

    printf("int_vector_merge_sort: %s\n", result);

    // Take this out for better testing.
    result = "Success";

    if (int_vector_dot_product(test_vector, test_vector) < int_vector_dot_product(test_vector, sorted_test_vector)) {
        result = "Failure";
    }

    printf("Calculation of minimum scalar product: %s\n", result);


    int_vector_destroy(merged_vector);
    int_vector_destroy(sorted_test_vector);
    int_vector_destroy(test_vector);



    /****************************
    ** Test 2
    *****************************/

    printf("\nTest 2...\n");

    test_vector = int_vector_create(3);
    int_vector_enter(test_vector, 0, 100);
    int_vector_enter(test_vector, 1, 5);
    int_vector_enter(test_vector, 2, 10);

    int entry_2;

    // Test min_int

    result = "Success";

    entry_0 = int_vector_entry(test_vector, 0);
    entry_1 = int_vector_entry(test_vector, 2);

    if (min_int(&entry_0, &entry_1) != 0 || min_int(&entry_1, &entry_0) != 1) {
        result = "Failure";
    }

    printf("min_int: %s\n", result);


    // Test int_vector_merge_sort

    result = "Success";

    sorted_test_vector = int_vector_merge_sort(test_vector, min_int);

    entry_0 = int_vector_entry(sorted_test_vector, 0);
    entry_1 = int_vector_entry(sorted_test_vector, 1);
    entry_2 = int_vector_entry(sorted_test_vector, 2);

    if (entry_0 != 5 || entry_1 != 10 || entry_2 != 100) {
        result = "Failure";
    }

    printf("int_vector_merge_sort: %s\n", result);


    int_vector_destroy(sorted_test_vector);
    int_vector_destroy(test_vector);


    return 0;
}
