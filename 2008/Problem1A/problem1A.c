
#include "minimum_scalar_product.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// next_int returns the next integer from a file of integers.
int next_int(FILE* ifp) {
    int value;
    fscanf(ifp, "%d", &value);
    return value;
}

int main(int argc, char* argv[]) {
    assert(argc == 3);

    char* in_file = argv[1];
    char* out_file = argv[2];

    FILE *ifp = fopen(in_file, "r");
    FILE *ofp = fopen(out_file, "w");

    int T = next_int(ifp);

    int n;
    int_vector *x, *y, *permuted_x, *permuted_y;
    long int dot_product;
    int i, j;

    for (i = 1; i <= T; i++) {
        n = next_int(ifp);

        x = int_vector_create(n);
        y = int_vector_create(n);

        for (j = 0; j < n; j++) {
            int_vector_enter(x, j, next_int(ifp));
        }
        for (j = 0; j < n; j++) {
            int_vector_enter(y, j, next_int(ifp));
        }

        permuted_x = int_vector_merge_sort(x, min_int);
        permuted_y = int_vector_merge_sort(y, max_int);

        dot_product = int_vector_dot_product(permuted_x, permuted_y);
        fprintf(ofp, "Case #%d: %ld\n", i, dot_product);

    }

    fclose(ifp);
    fclose(ofp);

    return 0;
}
