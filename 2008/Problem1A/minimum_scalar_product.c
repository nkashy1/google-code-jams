
#include "minimum_scalar_product.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>



int_vector* int_vector_create(int dimension) {
    int_vector *new_int_vector = malloc(sizeof(int_vector));
    new_int_vector->dimension = dimension;
    new_int_vector->entries = malloc(sizeof(int)*dimension);

    return new_int_vector;
}



void int_vector_destroy(int_vector *obsolete_int_vector) {
    assert(obsolete_int_vector != NULL);

    free(obsolete_int_vector->entries);
    free(obsolete_int_vector);
}



int int_vector_entry(int_vector *self, int position){
    assert(0 <= position && position <= self->dimension);

    return self->entries[position];
}



void int_vector_enter(int_vector *self, int position, int value){
    assert(0 <= position && position <= self->dimension);
    self->entries[position] = value;
}



// int_vector_merge:
// Description: Merge utility for int_vector_merge_sort.

int_vector* int_vector_merge(int_vector *vector_1, int_vector *vector_2, int (*order)(int*, int*)) {
    int_vector *merged_vector = int_vector_create(vector_1->dimension + vector_2->dimension);

    int i, *iterator_1, *iterator_2;
    iterator_1 = vector_1->entries;
    iterator_2 = vector_2->entries;

    for (i = 0; i < merged_vector->dimension; i++) {
        if (iterator_2 >= vector_2->entries + vector_2->dimension || (iterator_1 < vector_1->entries + vector_1->dimension && order(iterator_1, iterator_2) == 1)) {
            int_vector_enter(merged_vector, i, *iterator_1);
            iterator_1++;
        } else {
            int_vector_enter(merged_vector, i, *iterator_2);
            iterator_2++;
        }
    }

    int_vector_destroy(vector_1);
    int_vector_destroy(vector_2);

    return merged_vector;
}



int_vector* int_vector_merge_sort(int_vector *unsorted_vector, int (*order)(int*, int*)) {

    int_vector *sorted_vector;

    int num_sorted_subvectors = unsorted_vector->dimension;

    if (num_sorted_subvectors == 1) {
        return unsorted_vector;
    }

    int_vector* *sorted_subvectors;
    sorted_subvectors = malloc(sizeof(int_vector*)*num_sorted_subvectors);

    int i;
    int_vector *singleton;
    for (i = 0; i < num_sorted_subvectors; i++) {
        singleton = int_vector_create(1);
        int_vector_enter(singleton, 0, int_vector_entry(unsorted_vector, i));
        sorted_subvectors[i] = singleton;
    }

    int num_fresh_subvectors;
    int_vector* *fresh_subvectors;
    int current_sorted_subvectors[2];
    int current_fresh_subvector;

    while (num_sorted_subvectors > 1) {
        num_fresh_subvectors = num_sorted_subvectors - (num_sorted_subvectors/2); // Because the quotient is rounded down.
        fresh_subvectors = malloc(sizeof(int_vector*)*num_fresh_subvectors);

        current_sorted_subvectors[0] = 0;
        current_sorted_subvectors[1] = 1;
        current_fresh_subvector = 0;

        while (current_sorted_subvectors[1] < num_sorted_subvectors) {
            fresh_subvectors[current_fresh_subvector] = int_vector_merge(sorted_subvectors[current_sorted_subvectors[0]], sorted_subvectors[current_sorted_subvectors[1]], order);

            current_fresh_subvector++;
            current_sorted_subvectors[0] += 2;
            current_sorted_subvectors[1] += 2;
        }

        if (current_sorted_subvectors[0] < num_sorted_subvectors) {
            fresh_subvectors[current_fresh_subvector] = sorted_subvectors[current_sorted_subvectors[0]];
        }

        free(sorted_subvectors);
        sorted_subvectors = fresh_subvectors;
        num_sorted_subvectors = num_fresh_subvectors;
    }

    sorted_vector = *sorted_subvectors;
    free(sorted_subvectors);

    return sorted_vector;
}



int min_int(int *first, int *second) {
    int result = 0;

    if (*first <= *second) {
        result = 1;
    }

    return result;
}



int max_int(int *first, int *second) {
    int result = 0;

    if (*first >= *second) {
        result = 1;
    }

    return result;
}



long int int_vector_dot_product(int_vector *first_vector, int_vector *second_vector) {
    int dimension = first_vector->dimension;
    assert(dimension == second_vector->dimension);

    long int dot_product = 0;
    int i;
    for (i = 0; i < dimension; i++) {
        dot_product += ((long int) int_vector_entry(first_vector, i))*((long int) int_vector_entry(second_vector, i));
    }

    return dot_product;
}
