
/******************************************************************

Google Code Jam 2008, Problem A: Minimum Scalar Product
-------------------------------------------------------

We have two vectors, x = (x_1, x_2, ..., x_n) and y = (y_1, y_2, ...., y_n), with integer entries.

We want to find permutations r(k) and s(k) of {1, 2, ..., n} which minimize the
dot product x_{r(1)}y_{s(1)} + x_{r(2)}y_{s(2)} + ... + x_{r(n)}y_{s(n)}.

Input:  We receive test cases in text files.
        The first line of each file contains the number T of test cases in the file.
        For each test case, the first line contains the number n of components in the vectors.
        The next two lines respectively contain the vectors x and y for that test case.

Output: For each test case, the program should output a line
        "Case #k: m",
        where k is the number of the test case and m is the minimum scalar product for the vectors in that case.

Data sets:  (1) The small data set consists of T = 1000 test cases of dimensions n with 1 <= n <= 8 with components x_i and y_i between -1000 and 1000.
            (2) The large data set consists of T = 10 test cases of dimensions n with 100 <= n <= 800 with components x_i and y_i between -100000 and 100000.

*******************************************************************/



/*******************************************
** int_vector struct and related utilities
********************************************/

typedef struct int_vector int_vector;

struct int_vector {
    int dimension;
    int* entries;
};


int_vector* int_vector_create(int dimension);

void int_vector_destroy(int_vector* obsolete_int_vector);

int int_vector_entry(int_vector *self, int position);

void int_vector_enter(int_vector *self, int position, int value);



// int_vector_merge_sort
//
// Inputs:  (1) int_vector *unsorted_vector, a pointer to the int_vector to be sorted.
//          (2) int (*order)(int*, int*), a function pointer to a comparison function according to which unsorted_vector will be sorted.
//
// Output:  A pointer to a dynamically allocated int_vector which is the sorted version of unsorted_vector.
//
// Notes:
//  (1) The *order function takes two int pointers as arguments. It returns 1 if the integer at the first pointer location precedes the one at the second location in the ordering and 0 otherwise.

int_vector* int_vector_merge_sort(int_vector *unsorted_vector, int (*order)(int*, int*));

int_vector* int_vector_merge(int_vector *vector_1, int_vector *vector_2, int (*order)(int*, int*));


// min_int
//
// Inputs:  (1) int *first, a pointer to the first integer to be compared.
//          (2) int *second, a pointer to the second integer to be compared.
//
// Output: 1 if *first <= *second, 0 otherwise.
//
// Notes:

int min_int(int *first, int *second);



// max_int
//
// Inputs:  (1) int *first, a pointer to the first integer to be compared.
//          (2) int *second, a pointer to the second integer to be compared.
//
// Output: 1 if *first >= *second, 0 otherwise.
//
// Notes:

int max_int(int *first, int *second);



// int_vector_dot_product
//
// Inputs:  (1) int_vector *first_vector, a pointer to the first vector in the dot product.
//          (2) int_vector *second_vector, a pointer to the second vector in the dot product.
//
// Output: the integer which is the dot product of *first_vector and *second_vector.
//
// Notes:

long int int_vector_dot_product(int_vector *first_vector, int_vector *second_vector);
